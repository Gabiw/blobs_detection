import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import time
import cv2

import networkx as nx
import glob,sys,os
import datetime

from tqdm import tqdm
import json
from sklearn.model_selection import train_test_split

import pandas as pd
from copy import deepcopy


from skimage.util.shape import view_as_windows
from skimage.util import pad
from sklearn.feature_extraction.image import extract_patches_2d


### gw added
from skimage.filters import threshold_yen
from skimage.morphology import convex_hull_image
from skimage.filters import threshold_multiotsu
from skimage import util

from skimage import img_as_float
from skimage.util import invert

from skimage.filters import unsharp_mask
from skimage.filters import laplace
from skimage.exposure import match_histograms
from filter_image_utils import run_clean_img_rectangle_slices, image_bin_adjust_thershold, img_enhance, get_blob_img
from filter_image_utils import run_yen_on_image, get_contours, get_image_contours
from filter_image_utils import get_convexHull_img, debug_detect_crop, get_path_images, init_yen, createFolder
## end gw


run_debug = True
base_data_dir = 'C:/Users/gabiw/Senecio_robotics/Data/'
#output_dir = 'C:/Users/gabiw/Senecio_robotics/code/blobs_detection/mosquito_detection/'
output_dir = base_data_dir + 'full_image/out/'

class mosquito_blob_detection():
    def __init__(self, roi_size=300, scale_factor=1, gray_thr=70, blob_min_area=1000,
                 iou_thr=0.1, permit_range=[]):
        self.roi_size = roi_size
        self.scale_factor = scale_factor
        self.gray_thr = gray_thr
        self.blob_min_area = blob_min_area
        self.iou_thr=iou_thr
        self.permit_range=permit_range

    def get_iou(self,rec1,rec2):
        """
        Calculate the Intersection over Union (IoU) of two bounding boxes.

        Parameters
        ----------
        bb1 : dict
            Keys: {'x1', 'x2', 'y1', 'y2'}
            The (x1, y1) position is at the top left corner,
            the (x2, y2) position is at the bottom right corner
        bb2 : dict
            Keys: {'x1', 'x2', 'y1', 'y2'}
            The (x, y) position is at the top left corner,
            the (x2, y2) position is at the bottom right corner

        Returns
        -------
        float
            in [0, 1]
        """
        bb1 = {'x1':rec1[0],'y1':rec1[1],
               'x2':rec1[0]+self.roi_size,'y2':rec1[1]+self.roi_size}

        bb2 = {'x1':rec2[0],'y1':rec2[1],
               'x2':rec2[0]+self.roi_size,'y2':rec2[1]+self.roi_size}

        # determine the coordinates of the intersection rectangle
        x_left = max(bb1['x1'], bb2['x1'])
        y_top = max(bb1['y1'], bb2['y1'])
        x_right = min(bb1['x2'], bb2['x2'])
        y_bottom = min(bb1['y2'], bb2['y2'])

        if x_right < x_left or y_bottom < y_top:
            return 0.0

        # The intersection of two axis-aligned bounding boxes is always an
        # axis-aligned bounding box
        intersection_area = (x_right - x_left) * (y_bottom - y_top)

        # compute the area of both AABBs
        bb1_area = (bb1['x2'] - bb1['x1']) * (bb1['y2'] - bb1['y1'])
        bb2_area = (bb2['x2'] - bb2['x1']) * (bb2['y2'] - bb2['y1'])

        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = intersection_area / float(
            bb1_area + bb2_area - intersection_area)
        assert iou >= 0.0
        assert iou <= 1.0
        return iou



    def show_img_with_rois(self, I, detections, scaled_image=True,
                           figure=None):
        if figure is None:
            figure = plt.figure()
        ax = plt.axes()
        ax.imshow(I, cmap='gray')
        for k, roi in enumerate(detections):
            if scaled_image:
                rect = patches.Rectangle(roi,
                                         self.scale_factor * self.roi_size,
                                         self.scale_factor * self.roi_size,
                                         linewidth=1, edgecolor='r',
                                         facecolor='none')

            else:
                rect = patches.Rectangle([int(roi[0] / self.scale_factor),
                                          int(roi[1] / self.scale_factor)],
                                         self.roi_size, self.roi_size,
                                         linewidth=1, edgecolor='r',
                                         facecolor='none')
            ax.add_patch(rect)
        return figure

    def resize_image(self, img, method='scale'):
        img_out = img.copy()
        if not self.scale_factor == 1:
            if method == 'scale':
                img_out = cv2.resize(img, (0, 0), fx=self.scale_factor, fy=self.scale_factor)
            elif method == 'area':
                out_size = (int(self.scale_factor *img.shape[1]), int(self.scale_factor *img.shape[0]))  # (width, height)
                img_out = cv2.resize(img, out_size, interpolation=cv2.INTER_AREA)
            img_out = img_out.astype('uint8')
        return img_out


    def crop_detection(self, I, roi, scaled_image=True):
        h, w = I.shape
        if scaled_image:
            return I[max(roi[1], 0):min(roi[1] + int(self.scale_factor * self.roi_size), h-1),
                   max(roi[0], 0):min(roi[0] + int(self.scale_factor * self.roi_size), w-1)]
        else:
            return I[max(int(roi[1] / self.scale_factor), 0):min(int(roi[1] / self.scale_factor) + self.roi_size, h-1),
                   max(int(roi[0] / self.scale_factor), 0):min(int(roi[0] / self.scale_factor) + self.roi_size, w-1)]

    def find_duplicate_recs(self, detections, roi_size, thr=0.4):
        dups = []
        for i1 in range(len(detections)):
            for i2 in range(i1 + 1, len(detections)):
                rec1 = detections[i1]
                rec2 = detections[i2]
                if self.get_iou(rec1,rec2) > thr:
                    dups.append([i1, i2])
        return dups



    def reduce_duplicates_crops(self, detections, thr=0.1, roi_size=250):
        dup_crops = self.find_duplicate_recs(detections, roi_size=roi_size,
                                             thr=thr)
        all_dup_crops_inds = np.union1d(dup_crops, dup_crops)
        non_dup_crops = np.setdiff1d(np.arange(0, len(detections)),
                                     all_dup_crops_inds)
        G = nx.Graph()
        G.add_edges_from(dup_crops)
        cliques = nx.clique.find_cliques(G)
        reduced_detections = [detections[i] for i in non_dup_crops]
        for clique in cliques:
            clique_center = np.array([0, 0])
            for k in clique:
                clique_center = clique_center + np.array(detections[k])

            reduced_detections.append(
                list((clique_center / len(clique)).astype('int16')))

        return reduced_detections

    def reduce_duplicate_crops_envelope(self, detections, I_shape,
                                        recursion_counter,
                                        max_crops_for_reduction_alg=100,
                                        thr=0.1, roi_size=250):
        recursion_counter = recursion_counter + 1
        # print(recursion_counter)
        if recursion_counter < 10:
            if len(detections) > max_crops_for_reduction_alg:  # if too many crops the clique algorithm will be too long, then split image into 4 quorters
                detections1 = []
                detections2 = []
                detections3 = []
                detections4 = []
                for d in detections:
                    if d[0] <= I_shape[0] // 2 and d[1] <= I_shape[1] // 2:
                        detections1.append(d)
                    if d[0] <= I_shape[0] // 2 and d[1] > I_shape[1] // 2:
                        detections2.append(d)
                    if d[0] > I_shape[0] // 2 and d[1] <= I_shape[1] // 2:
                        detections3.append(d)
                    if d[0] > I_shape[0] // 2 and d[1] > I_shape[1] // 2:
                        detections4.append(d)

                detections1 = self.reduce_duplicate_crops_envelope(detections1,
                                                                   list(np.array(I_shape) // 2),
                                                                   recursion_counter,
                                                                   max_crops_for_reduction_alg,
                                                                   thr=thr,
                                                                   roi_size=roi_size)
                detections2 = self.reduce_duplicate_crops_envelope(detections2,
                                                                   list(np.array( I_shape) // 2),
                                                                   recursion_counter,
                                                                   max_crops_for_reduction_alg,
                                                                   thr=thr,
                                                                   roi_size=roi_size)
                detections3 = self.reduce_duplicate_crops_envelope(detections3,
                                                                   list(np.array(I_shape) // 2),
                                                                   recursion_counter,
                                                                   max_crops_for_reduction_alg,
                                                                   thr=thr,
                                                                   roi_size=roi_size)
                detections4 = self.reduce_duplicate_crops_envelope(detections4,
                                                                   list(np.array(I_shape) // 2),
                                                                   recursion_counter,
                                                                   max_crops_for_reduction_alg,
                                                                   thr=thr,
                                                                   roi_size=roi_size)
                detections = detections1 + detections2 + detections3 + detections4
            else:
                detections = self.reduce_duplicates_crops(detections, thr=thr, roi_size=roi_size)

        return detections

    def reduce_duplicates_blobs(self, blobs_vec, mbr_vec, diag_vec, centers_vec, w_h_vec, thr=0.1, roi_size=250):
        dup_crops = self.find_duplicate_recs(blobs_vec, roi_size=roi_size,
                                             thr=thr)
        all_dup_crops_inds = np.union1d(dup_crops, dup_crops)
        non_dup_crops = np.setdiff1d(np.arange(0, len(blobs_vec)),
                                     all_dup_crops_inds)
        G = nx.Graph()
        G.add_edges_from(dup_crops)
        cliques = nx.clique.find_cliques(G)
        reduced_blobs_vec = [blobs_vec[i] for i in non_dup_crops]
        reduced_mbrs = [mbr_vec[i] for i in non_dup_crops]
        reduced_diags = [diag_vec[i] for i in non_dup_crops]
        reduced_cogs = [centers_vec[i] for i in non_dup_crops]
        reduced_w_hs = [w_h_vec[i] for i in non_dup_crops]
        for clique in cliques:
            clique_center = np.array([0]*len(blobs_vec[0]))
            clique_mbrs = np.array([0]*len(mbr_vec[0]))
            clique_diags = np.array(0, dtype=np.float)
            clique_cogs = np.array([0]*len(centers_vec[0]), dtype=np.float)
            clique_whs = np.array([0]*len(w_h_vec[0]), dtype=np.float)
            for k in clique:
                clique_center = clique_center + np.array(blobs_vec[k])
                clique_mbrs = clique_mbrs + np.array(mbr_vec[k])
                clique_diags = clique_diags + np.array(diag_vec[k])
                clique_cogs = clique_cogs + np.array(centers_vec[k])
                clique_whs = clique_whs + np.array(w_h_vec[k])

            reduced_blobs_vec.append(list((clique_center / len(clique)).astype('int16')))
            reduced_mbrs.append(tuple(np.array(clique_mbrs/len(clique)+0.5*np.ones(len(clique_mbrs)), dtype=np.int16)))
            reduced_diags.append(float(clique_diags / len(clique)))
            reduced_cogs.append(tuple(clique_cogs / len(clique)))
            reduced_w_hs.append(tuple(clique_whs / len(clique)))


        #blobs_vec, all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec

        return reduced_blobs_vec, reduced_mbrs, reduced_diags, reduced_cogs, reduced_w_hs

    def reduce_duplicate_blobs_envelope(self, blobs_vec, all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec,
                                            I_shape, recursion_counter, max_crops_for_reduction_alg=100,
                                            thr=0.1, roi_size=250):
        recursion_counter = recursion_counter + 1
        # print(recursion_counter)
        if recursion_counter < 10:
            if len(blobs_vec) > max_crops_for_reduction_alg:  # if too many crops the clique algorithm will be too long, then split image into 4 quorters
                blobs_vec1 = []
                blobs_vec2 = []
                blobs_vec3 = []
                blobs_vec4 = []
                #all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec
                all_mbr_vec1 = []
                all_mbr_vec2 = []
                all_mbr_vec3 = []
                all_mbr_vec4 = []
                all_diag_vec1 = []
                all_diag_vec2 = []
                all_diag_vec3 = []
                all_diag_vec4 = []
                all_centers_vec1 = []
                all_centers_vec2 = []
                all_centers_vec3 = []
                all_centers_vec4 = []
                all_w_h_vec1 = []
                all_w_h_vec2 = []
                all_w_h_vec3 = []
                all_w_h_vec4 = []
                for index, d in enumerate(blobs_vec):
                    if d[0] <= I_shape[0] // 2 and d[1] <= I_shape[1] // 2: # first quarter
                        blobs_vec1.append(d)
                        all_mbr_vec1.append(all_mbr_vec[index])
                        all_diag_vec1.append(all_diag_vec[index])
                        all_centers_vec1.append(all_centers_vec[index])
                        all_w_h_vec1.append(all_w_h_vec[index])
                    if d[0] <= I_shape[0] // 2 and d[1] > I_shape[1] // 2: # second quarter
                        blobs_vec2.append(d)
                        all_mbr_vec2.append(all_mbr_vec[index])
                        all_diag_vec2.append(all_diag_vec[index])
                        all_centers_vec2.append(all_centers_vec[index])
                        all_w_h_vec2.append(all_w_h_vec[index])
                    if d[0] > I_shape[0] // 2 and d[1] <= I_shape[1] // 2: # third quarter
                        blobs_vec3.append(d)
                        all_mbr_vec3.append(all_mbr_vec[index])
                        all_diag_vec3.append(all_diag_vec[index])
                        all_centers_vec3.append(all_centers_vec[index])
                        all_w_h_vec3.append(all_w_h_vec[index])
                    if d[0] > I_shape[0] // 2 and d[1] > I_shape[1] // 2:# forth quarter
                        blobs_vec4.append(d)
                        all_mbr_vec4.append(all_mbr_vec[index])
                        all_diag_vec4.append(all_diag_vec[index])
                        all_centers_vec4.append(all_centers_vec[index])
                        all_w_h_vec4.append(all_w_h_vec[index])

                blobs_vec1, all_mbr_vec1, all_diag_vec1, all_centers_vec1, all_w_h_vec1 = \
                    self.reduce_duplicate_blobs_envelope(blobs_vec1, all_mbr_vec1, all_diag_vec1,
                                                             all_centers_vec1, all_w_h_vec1,
                                                             list(np.array(I_shape) // 2),
                                                             recursion_counter,
                                                             max_crops_for_reduction_alg,
                                                             thr=thr,
                                                             roi_size=roi_size)

                blobs_vec2, all_mbr_vec2, all_diag_vec2, all_centers_vec2, all_w_h_vec2 = \
                    self.reduce_duplicate_blobs_envelope(blobs_vec2, all_mbr_vec2, all_diag_vec2,
                                                             all_centers_vec2, all_w_h_vec2,
                                                             list(np.array( I_shape) // 2),
                                                             recursion_counter,
                                                             max_crops_for_reduction_alg,
                                                             thr=thr,
                                                             roi_size=roi_size)

                blobs_vec3, all_mbr_vec3, all_diag_vec3, all_centers_vec3, all_w_h_vec3 = \
                    self.reduce_duplicate_blobs_envelope(blobs_vec3, all_mbr_vec3, all_diag_vec3,
                                                             all_centers_vec3, all_w_h_vec3,
                                                             list(np.array(I_shape) // 2),
                                                             recursion_counter,
                                                             max_crops_for_reduction_alg,
                                                             thr=thr,
                                                             roi_size=roi_size)

                blobs_vec4, all_mbr_vec4, all_diag_vec4, all_centers_vec4, all_w_h_vec4 = \
                    self.reduce_duplicate_blobs_envelope(blobs_vec4, all_mbr_vec4, all_diag_vec4,
                                                             all_centers_vec4, all_w_h_vec4,
                                                             list(np.array(I_shape) // 2),
                                                             recursion_counter,
                                                             max_crops_for_reduction_alg,
                                                             thr=thr,
                                                             roi_size=roi_size)

                blobs_vec = blobs_vec1 + blobs_vec2 + blobs_vec3 + blobs_vec4
                all_mbr_vec  = all_mbr_vec1 + all_mbr_vec2 + all_mbr_vec3 + all_mbr_vec4
                all_diag_vec  = all_diag_vec1 + all_diag_vec2 + all_diag_vec3 + all_diag_vec4
                all_centers_vec  = all_centers_vec1 + all_centers_vec2 + all_centers_vec3 + all_centers_vec4
                all_w_h_vec  = all_w_h_vec1 + all_w_h_vec2 + all_w_h_vec3 + all_w_h_vec4
            else:
                blobs_vec, all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec = \
                    self.reduce_duplicates_blobs(blobs_vec, all_mbr_vec, all_diag_vec, all_centers_vec,
                                                     all_w_h_vec, thr=thr, roi_size=roi_size)

        return blobs_vec, all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec


    def detect(self, img, show_results=False, reduce_duplicates=True):
        global output_dir
        start_time = time.time()
        #dims = len(np.shape(img))

        img = self.resize_image(img, method='scale')
#        cmethod = 'histeq'
        cmethod = 'histeq' #'orig'
        img = img_enhance(img, method=cmethod)

        if run_debug:
            enhance_img_name = cmethod + '_img_enhance.jpg'
            isWritten = cv2.imwrite(os.path.join(output_dir, enhance_img_name), img)
            if not isWritten:
                print('Error: The image is not saved. img_enhance.shape = ', img.shape)

        #img = cv2.equalizeHist(img)
        #img = cv2.medianBlur(img, 7)
        #try adaptive saf
        #I2 = (img < self.gray_thr).astype('uint8')
        cmethod = 'fix'
        bin_img, self.gray_thr = image_bin_adjust_thershold(img, self.gray_thr, method=cmethod)

        if run_debug:
            bin_img_name = cmethod + '_' + str(self.gray_thr) + '_bin_img'  + '.jpg'
            isWritten = cv2.imwrite(os.path.join(output_dir, bin_img_name), bin_img*255)
            if not isWritten:
                print('Error: The image is not saved. bin image.shape = ', bin_img.shape)


        #----- tring yen's_method--------
        # bin_img = (img <=threshold_otsu(img) ).astype('uint8')
        #contours, hierarchy = cv2.findContours(bin_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        crun_tree = False
        contours, hierarchy = get_contours(bin_img, run_tree=crun_tree)

        cshape = (img.shape[0], img.shape[1], 3)
        if run_debug:
            img_contours = get_image_contours(cshape, contours, fill_cont=False)
            contour_img_name = cmethod + '_contour_img.jpg'
            isWritten = cv2.imwrite(os.path.join(output_dir, contour_img_name), img_contours.astype('uint8'))
            if not isWritten:
                print('Error: The image is not saved. img_contours.shape = ', img_contours.shape)
            img_contours = get_image_contours(cshape, contours, fill_cont=True)
            contour_img_name = cmethod + '_fill_contour_img.jpg'
            isWritten = cv2.imwrite(os.path.join(output_dir, contour_img_name), img_contours)
            if not isWritten:
                print('Error: The image is not saved. fill img_contours.shape = ', img_contours.shape)

#size=250,gray=20,iou_thr=0.3,area=700
        large_cc = [cnt for cnt in contours if cv2.contourArea(cnt) > self.blob_min_area]
        add_box = True
        if len(large_cc) == 0 and add_box:
            min_area_saf = 1.5
            area_vec = [cv2.contourArea(cnt) for cnt in contours if cv2.contourArea(cnt) > min_area_saf]
            ind_area_vec = [index for index, cnt in enumerate(contours) if cv2.contourArea(cnt) > min_area_saf]
            ranked = np.argsort(area_vec)
            max_cont = min(3,int(0.25*len(area_vec)))
            max_cont = min(max_cont, len(area_vec))
            largest_indices = ranked[::-1][:max_cont]
            ind_large_cc = [ind_area_vec[ind] for ind in largest_indices]
            large_cc = [contours[ind] for ind in ind_large_cc]
            add_box = False
        elif len(large_cc) > 0 :
            add_box = True
        recs = [cv2.boundingRect(cnt) for cnt in large_cc]
        mbr_vec = [cv2.minAreaRect(cnt) for cnt in large_cc]
        box_mbr_vec = [cv2.boxPoints(rect) for rect in mbr_vec]# if rect[2]*rect[3] >> self.blob_min_area]
        int_mbr_vec = [np.int0(box) for box in box_mbr_vec]

        #rect = cv2.minAreaRect(cnt)
        #box = cv2.boxPoints(rect)
        #box = np.int0(box)
        #im = cv2.drawContours(im, [box], 0, (0, 0, 255), 2)
        #add_box = False
        all_mbr_vec = []
        if run_debug and add_box:
            img_contours = get_image_contours(cshape, large_cc, fill_cont=False)
            contour_img_name = cmethod + '_large_contour_img.jpg'
            isWritten = cv2.imwrite(os.path.join(output_dir, contour_img_name), img_contours)
            if not isWritten:
                print('Error: The image is not saved. img_contours.shape = ', img_contours.shape)
            img_contours = get_image_contours(cshape, large_cc, fill_cont=True)
            contour_img_name = cmethod + '_fill_large_contour_img.jpg'
            isWritten = cv2.imwrite(os.path.join(output_dir, contour_img_name), img_contours)
            if not isWritten:
                print('Error: The image is not saved. fill img_contours.shape = ', img_contours.shape)
            for ii, clnt in enumerate(large_cc):
                #x, y, w, h = cv2.boundingRect(cnt)
                clrec = recs[ii]
                cmbr = mbr_vec[ii]
                cbox = box_mbr_vec[ii]
                cbox_int = int_mbr_vec[ii]
                print('ii, clrec, cmbr, cbox_int = ', ii, clrec, cmbr[:2], cbox_int)
                x0 = clrec[0]
                y0 = clrec[1]
                w = clrec[2]
                h = clrec[3]
                xe = x0+w
                ye = y0+h
                #lcimg = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
                lcimg = img[y0:ye, x0:xe]
                cshape = (lcimg.shape[0], lcimg.shape[1], 3)
                shft = np.array([x0, y0], dtype=np.int32)
                nclnt = clnt - shft
                img_contours_diff = get_image_contours(cshape, nclnt, fill_cont=False)
                chull_diff, chull, binary = get_convexHull_img(img_contours_diff)
                img_name = cmethod + '_convexHull_img_'+ str(ii) + '_'+ str(h) + '_'+ str(w) + '.jpg'
                isWritten = cv2.imwrite(os.path.join(output_dir, img_name), 255*chull.astype('uint8'))
                if not isWritten:
                    print('Error: The image is not saved. chull.shape = ', chull_diff.shape)
                img_name = cmethod + '_diff_convexHull_img_'+ str(ii) + '_'+ str(h) + '_'+ str(w) + '.jpg'
                cfile = os.path.join(output_dir, img_name)
                #if not os.path.isfile(cfile):
                cval_max = np.max(chull_diff)
                cval_min = np.min(chull_diff)
                chull_diff = np.int32(255 * (chull_diff - cval_min) / (cval_max - cval_min) + cval_min + 0.5)
                isWritten = cv2.imwrite(cfile, chull_diff.astype('uint8'))
                if not isWritten:
                    print('Error: The image is not saved. chull_diff.shape = ', chull_diff.shape)

                blob_img, minmax_vec = get_blob_img(img, chull, binary, shft)
                #minmax_vec = (rmin, rmax, cmin, cmax)
                all_mbr_vec += [minmax_vec]
                img_name = cmethod + '_blob_img_'+ str(ii) + '_'+ str(h) + '_'+ str(w) + '.jpg'
                cfile = os.path.join(output_dir, img_name)
                #if not os.path.isfile(cfile):
                isWritten = cv2.imwrite(cfile, blob_img)
                if not isWritten:
                    print('Error: The image is not saved. blob_img.shape = ', blob_img.shape)

        detections = []
        for r in recs:
            detections.append(
                [r[0] + r[2] // 2 - self.scale_factor * self.roi_size // 2,
                 r[1] + r[3] // 2 - self.scale_factor * self.roi_size // 2])

        if not self.permit_range == []:
            detections = [d for d in detections if
                          d[0] >= self.permit_range[0] and d[0] <= self.permit_range[1] and
                          d[1] >= self.permit_range[2] and d[1] <= self.permit_range[3]]

        if reduce_duplicates:
            # max_crops_for_reduction_alg=80 after time measuring optimization
            # self.show_img_with_rois(img, detections)
            detections = self.reduce_duplicate_crops_envelope(detections,
                                                              img.shape[:2], 0,
                                                              max_crops_for_reduction_alg=80,
                                                              thr=self.iou_thr,
                                                              roi_size=self.roi_size)
            detections = self.reduce_duplicate_crops_envelope(detections,
                                                              img.shape[:2], 0,
                                                              max_crops_for_reduction_alg=80,
                                                              thr=self.iou_thr,
                                                              roi_size=self.roi_size)

        if show_results:# or run_debug:
            print("Total Time: " + str(time.time() - start_time))
            self.show_img_with_rois(img, detections)
            plt.show()

        return detections


    def extract_blobs_from_img(self, img, show_results=False, reduce_duplicates=True):
        def dist_points(p1, p2):
            if not isinstance(p1, np.ndarray):
                p1 = np.array(p1)
            if not isinstance(p2, np.ndarray):
                p2 = np.array(p2)
            # finding sum of squares
            sum_sq = np.sum(np.square(p1 - p2))
            return np.sqrt(sum_sq)  # np.sqrt(p0[0]*p1[0]+p0[1]*p1[1])

        global output_dir
        start_time = time.time()
        #dims = len(np.shape(img))

        img = self.resize_image(img, method='scale')
#        cmethod = 'histeq'
        cmethod = 'orig' #'histeq' #'unsharp' #'CLAHE' #'sharp_lap'  #'orig'
        img = img_enhance(img, method=cmethod)

        if run_debug and cmethod == 'orig':
            enhance_img_name = cmethod + '_img_enhance.jpg'
            isWritten = cv2.imwrite(os.path.join(output_dir, enhance_img_name), img)
            if not isWritten:
                print('Error: The image is not saved. img_enhance.shape = ', img.shape)

        #img = cv2.equalizeHist(img)
        #img = cv2.medianBlur(img, 7)
        #try adaptive saf
        #I2 = (img < self.gray_thr).astype('uint8')
        cmethod = "yen" #'otsu_small_blobs' #'fix'
        #clean_img = filter_patch(cimg, method=cmethod)
        bin_img, self.gray_thr = image_bin_adjust_thershold(img, self.gray_thr, method=cmethod)
        print(f"\n method, self.gray_thr = , {cmethod}, {self.gray_thr}")
        clean_img = util.invert(bin_img*util.invert(bin_img*img))
        #matched_img = match_histograms(clean_img, img, multichannel=True)
        if run_debug:
            #bin_img_name = cmethod + '_' + str(self.gray_thr) + '_bin_img'  + '.jpg'
            #isWritten = cv2.imwrite(os.path.join(output_dir, bin_img_name), bin_img*255)
            #if not isWritten:
            #    print('Error: The image is not saved. bin image.shape = ', bin_img.shape)

            clean_img_name = cmethod + '_' + str(self.gray_thr) + '_clean_img'  + '.jpg'
            isWritten = cv2.imwrite(os.path.join(output_dir, clean_img_name), clean_img.astype('uint8'))
            if not isWritten:
                print('Error: The image is not saved. bin image.shape = ', bin_img.shape)
            #match_img_name = cmethod + '_' + str(self.gray_thr) + '_matched_img'  + '.jpg'
            #isWritten = cv2.imwrite(os.path.join(output_dir, match_img_name), matched_img.astype('uint8'))
            #if not isWritten:
            #    print('Error: The image is not saved. matched_img .shape = ', matched_img.shape)


        #----- tring yen's_method--------
        # bin_img = (img <=threshold_otsu(img) ).astype('uint8')
        #contours, hierarchy = cv2.findContours(bin_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        crun_tree = False #True #
        contours, hierarchy = get_contours(bin_img, run_tree=crun_tree)

        cshape = (img.shape[0], img.shape[1], 3)
        if run_debug:
            img_contours = get_image_contours(cshape, contours, fill_cont=False)
            contour_img_name = cmethod + '_contour_img.jpg'
            isWritten = cv2.imwrite(os.path.join(output_dir, contour_img_name), img_contours.astype('uint8'))
            if not isWritten:
                print('Error: The image is not saved. img_contours.shape = ', img_contours.shape)
            img_contours = get_image_contours(cshape, contours, fill_cont=True)
            contour_img_name = cmethod + '_fill_contour_img.jpg'
            isWritten = cv2.imwrite(os.path.join(output_dir, contour_img_name), img_contours)
            if not isWritten:
                print('Error: The image is not saved. fill img_contours.shape = ', img_contours.shape)

#size=250,gray=20,iou_thr=0.3,area=700
        large_cc = [cnt for cnt in contours if cv2.contourArea(cnt) > self.blob_min_area]
        #add_box = True
        if len(large_cc) == 0:# and add_box:
            min_area_saf = 5.0
            area_vec = [cv2.contourArea(cnt) for cnt in contours if cv2.contourArea(cnt) > min_area_saf]
            ind_area_vec = [index for index, cnt in enumerate(contours) if cv2.contourArea(cnt) > min_area_saf]
            ranked = np.argsort(area_vec)
            max_cont = min(3,int(0.25*len(area_vec)))
            max_cont = min(max_cont, len(area_vec))
            largest_indices = ranked[::-1][:max_cont]
            ind_large_cc = [ind_area_vec[ind] for ind in largest_indices]
            large_cc = [contours[ind] for ind in ind_large_cc]
            #add_box = False
        #elif len(large_cc) > 0 :
        #    add_box = True

        recs = [cv2.boundingRect(cnt) for cnt in large_cc]
        mbr_vec = [cv2.minAreaRect(cnt) for cnt in large_cc]
        #cv2.minAreaRect(cnt) => [cx, cy, w, h, theta]
        box_mbr_vec = [cv2.boxPoints(rect) for rect in mbr_vec]# if rect[2]*rect[3] >> self.blob_min_area]
        int_mbr_vec = [np.int0(box) for box in box_mbr_vec]

        #rect = cv2.minAreaRect(cnt)
        #box = cv2.boxPoints(rect)
        #box = np.int0(box)
        #im = cv2.drawContours(im, [box], 0, (0, 0, 255), 2)
        #add_box = False
        if run_debug:# and add_box:
            img_contours = get_image_contours(cshape, large_cc, fill_cont=False)
            contour_img_name = cmethod + '_large_contour_img.jpg'
            isWritten = cv2.imwrite(os.path.join(output_dir, contour_img_name), img_contours)
            if not isWritten:
                print('Error: The image is not saved. img_contours.shape = ', img_contours.shape)
            img_contours = get_image_contours(cshape, large_cc, fill_cont=True)
            contour_img_name = cmethod + '_fill_large_contour_img.jpg'
            isWritten = cv2.imwrite(os.path.join(output_dir, contour_img_name), img_contours)
            if not isWritten:
                print('Error: The image is not saved. fill img_contours.shape = ', img_contours.shape)

        all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec = get_mbrs_data(large_cc, recs, mbr_vec, int_mbr_vec, img, clean_img)


        blobs_vec = []
        for r in recs:
            blobs_vec.append(
                [r[0] + r[2] // 2 - self.scale_factor * self.roi_size // 2,
                 r[1] + r[3] // 2 - self.scale_factor * self.roi_size // 2])

        if not self.permit_range == []:
            #blobs_vec = [d for d in blobs_vec if
            #              d[0] >= self.permit_range[0] and d[0] <= self.permit_range[1] and
            #              d[1] >= self.permit_range[2] and d[1] <= self.permit_range[3]]
            ind_det_vec = []
            for index, d in enumerate(blobs_vec):
                if d[0] >= self.permit_range[0] and d[0] <= self.permit_range[1] and \
                        d[1] >= self.permit_range[2] and d[1] <= self.permit_range[3]:
                    ind_det_vec.append(index)

            if len(ind_det_vec) < len(blobs_vec):
                blobs_vec = [blobs_vec[ii] for ii in ind_det_vec]
                all_mbr_vec = [all_mbr_vec[ii] for ii in ind_det_vec]
                all_diag_vec = [all_diag_vec[ii] for ii in ind_det_vec]
                all_centers_vec = [all_centers_vec[ii] for ii in ind_det_vec]
                all_w_h_vec = [all_w_h_vec[ii] for ii in ind_det_vec]


        if reduce_duplicates:
            # max_crops_for_reduction_alg=80 after time measuring optimization
            # self.show_img_with_rois(img, blobs_vec)
            #tmp_dts = blobs_vec.copy()
            blobs_vec, all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec = \
                self.reduce_duplicate_blobs_envelope(blobs_vec, all_mbr_vec, all_diag_vec, all_centers_vec,
                                                         all_w_h_vec,
                                                         img.shape[:2], 0,
                                                         max_crops_for_reduction_alg=80,
                                                         thr=self.iou_thr,
                                                         roi_size=self.roi_size)

            blobs_vec, all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec = \
                self.reduce_duplicate_blobs_envelope(blobs_vec, all_mbr_vec, all_diag_vec, all_centers_vec,
                                                         all_w_h_vec,
                                                         img.shape[:2], 0,
                                                         max_crops_for_reduction_alg=80,
                                                         thr=self.iou_thr,
                                                         roi_size=self.roi_size)

        if show_results:# or run_debug:
            print("Total Time: " + str(time.time() - start_time))
            self.show_img_with_rois(img, blobs_vec)
            plt.show()
            #clean_img
            print("Total Time: " + str(time.time() - start_time))
            self.show_img_with_rois(clean_img, blobs_vec)
            plt.show()

        return blobs_vec, all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec, clean_img



# --- new code ----


def label_predict(model, imgs, labels):
    """ tmp method, already implemented in the object_detction inference package"""
    preds = []
    if len(imgs) > 0:
        pred_proba = model.predict(imgs)
        y_pred_batch = np.argmax(pred_proba, axis=1)
        inv_map = {k: v for k, v in enumerate(labels)}
        preds = np.vectorize(inv_map.get)(y_pred_batch)
    return preds






def sliding_window_create(img_path,dest_path,window_shape=(800,600),step_overlap=200):
    """
    create and save image block via sliding window and save in dest path
    :param img_path: the path of the source full image to crop
    :param dest_path: where to save the images
    :param window_shape: window shape of the crop e.g (800,600) or 600 for a
     square
    :param step: step size between windows, e.g (800,600) or 600 for a same 600
    step in x and y
    :return:
    """
    step = []
    for i in range(len(window_shape)):
        if i < 2:
            step.append(window_shape[i]-step_overlap)
        else:
            step.append(window_shape[i])

    img_name = os.path.basename(img_path)
    img_base_name,extention = os.path.splitext(img_name)
    img = cv2.imread(img_path)
    B = view_as_windows(img,window_shape=window_shape , step=step)
    windows = B.reshape(-1, *img.shape)

    for i,window in enumerate(windows):
        curr_name = img_base_name + f"_Block_{i}"+extention
        curr_dest_path = os.path.join(dest_path,curr_name)
        cv2.imwrite(curr_dest_path,window)

def create_blocks_from_src_images(src_path, dest_path, window_shape, step_overlap):
    """
    A method to iterate throught images in a source path and create blocks
    from each image
    :param src_path:
    :param dest_path:
    :param window_shape:
    :param step_overlap:
    :return:
    """
    start_time = datetime.datetime.now()
    current_time = start_time.strftime("%d_%m_%y__%H_%M_%S")
    dest_path = os.path.join(dest_path,current_time)

    createFolder(dest_path)

    for im_path in tqdm(glob.glob(os.path.join(src_path, '*'))):
        sliding_window_create(im_path, dest_path, window_shape, step_overlap)





def check_coord(coord,window_size,img_size):
    crop_size = window_size
    if coord < 0:
        crop_size = window_size + coord
        coord = 0

    elif img_size - (coord + window_size) < 0:  #check right border
        crop_size = img_size - coord - 1

    return coord,crop_size


def get_pad_size(img,window_size,step_size,axis):
    """
    determine how much to pad image for the patching on a given axis
    :param img:
    :param window_size:
    :param step_size:
    :param axis:
    :return:
    """
    img_dim = img.shape[axis]
    window_dim = window_size[axis]
    step_dim = step_size[axis]

    last_coord = img_dim - window_dim
    modolu = last_coord % step_dim
    if modolu == 0:
        pad_size = 0
    else:
        multiple = step_dim * (last_coord // step_dim +1)
        pad_size = multiple - last_coord
    return pad_size



def is_rec_in_slice(slice_w,slice_h,x_s,y_s,x_b,y_b,w,h,img_name):

    # if img_name == '01_02_20__01_08_14_start__cycle_387 _Not_Empty_Block_17':
    #     print()
    is_in_slice = False
    ret_x = x_b - x_s
    ret_y = y_b - y_s
    ret_w = w
    ret_h = h

    if ret_x < 0 and ret_x + w > 0:
        ret_w += ret_x
        ret_x = 0

    # elif ret_x > slice_w and ret_x - w <= slice_w:
    #     w-= ret_x-slice_w
    #     ret_x = slice_w

    if ret_y < 0 and ret_y + h > 0:
        ret_h += ret_y
        ret_y = 0

    # elif ret_y > slice_h and ret_y - h <= slice_h:
    #     h-= ret_y-slice_h
    #     ret_y = slice_h

    if 0<=ret_x<=slice_w and 0 <= ret_y <= slice_h:

        ret_x, ret_w = check_coord(ret_x, ret_w, slice_w)
        ret_y, ret_h = check_coord(ret_y, ret_h, slice_h)

        if ret_h > h/2 and ret_w > w/2:
            is_in_slice = True

    return ret_x,ret_y,ret_w,ret_h,is_in_slice


def slice_dataset(df_path,imgs_path,dest_path,window_shape,window_ovelap,visualize=False):
    df = pd.read_csv(df_path)
    ret_df = pd.DataFrame(columns=df.columns)
    img_dest_path = os.path.join(dest_path,'imgs')
    if not os.path.exists(img_dest_path):
        createFolder(img_dest_path)
    for group in tqdm(df.groupby('image_id')):
        img_name = group[0] + '.jpg'
        img_path = os.path.join(imgs_path, img_name)
        img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)

        y_slicesize = window_shape[0] # h
        x_slicesize = window_shape[1] # w

        y_starts, x_starts = __rectangle_slices_indices(img, y_slicesize,
                                                        x_slicesize, window_ovelap)

        i = 0
        for y in y_starts:
            for x in x_starts:
                curr_coords = group[1].copy()
                curr_coords['is_in_slice'] = False
                slice = img[y:y + y_slicesize, x:x + x_slicesize].copy()
                slice_h = slice.shape[0]
                slice_w = slice.shape[1]

                curr_id = group[0] + f'_Block_{i}'
                i+=1
                if curr_id == r'02_02_20__16_30_35_start__cycle_697 _Not_Empty_Block_3':
                    plt.imshow(slice,cmap='gray')
                    plt.show()


                curr_dest_path = os.path.join(img_dest_path, curr_id +'.jpg')
                # cv2.imwrite(curr_dest_path, slice) # save all the images?

                curr_coords['x'], curr_coords['y'], curr_coords['w'], \
                curr_coords['h'],curr_coords['is_in_slice'] = zip(*curr_coords.apply(lambda z: is_rec_in_slice(slice_w,
                                                                           slice_h,
                                                                           x,y,
                                                                           z['x'],
                                                                           z['y'],
                                                                           z['w'],
                                                                           z['h'],
                                                                            curr_id),axis=1))
                curr_coords = curr_coords[curr_coords['is_in_slice'] == True]
                curr_coords['width'] = slice_w
                curr_coords['height'] = slice_h
                curr_coords['image_id'] = curr_id
                curr_coords.drop(columns='is_in_slice',inplace=True)

                if len(curr_coords) > 0:
                    ret_df = ret_df.append(curr_coords,ignore_index=True)
                    if visualize:
                        for j in range(len(curr_coords)):
                            x1 = curr_coords.iloc[j]['x']
                            y1 = curr_coords.iloc[j]['y']
                            x2 = x1 + curr_coords.iloc[j]['w']
                            y2 = y1 + curr_coords.iloc[j]['h']

                            p1 = (x1, y1)
                            p2 = (x2, y2)
                            cv2.rectangle(slice, p1, p2, (0, 0, 255), 10)

                    cv2.imwrite(curr_dest_path, slice)
        ret_df.to_csv(os.path.join(dest_path,'annotations.csv'),index=False)


def __rectangle_slices_indices(img, y_slicesize, x_slicesize, overlap):

    width = img.shape[1]
    height = img.shape[0]
    y_starts = list(range(0, height - y_slicesize + 1, y_slicesize-overlap))
    if len(y_starts) > 0 and y_starts[len(y_starts)-1] < height - y_slicesize - 1:
        y_starts.append(height - y_slicesize)
    x_starts = list(range(0, width - x_slicesize + 1, x_slicesize-overlap))
    if len(x_starts) > 0 and x_starts[len(x_starts)-1] < width - x_slicesize - 1:
        x_starts.append(width - x_slicesize)

    return y_starts,x_starts


def patch_image(img,img_name,curr_image_coords,window_shape,step_overlap,dest_path):
    """
    turn image with object detection annotations into a set of patches
    :param img:
    :param img_name:
    :param curr_image_coords:
    :param window_shape:
    :param step_overlap:
    :return:
    """
    image_base_name,extention = os.path.splitext(img_name)

    ret_df = pd.DataFrame(columns=curr_image_coords.columns)
    B, step, windows = slice_image_with_padding(img, step_overlap, window_shape)


    for i, window in enumerate(windows):
        curr_name = image_base_name + f'_Block_{i}'
        curr_dest_path = os.path.join(dest_path, curr_name + extention)
        cv2.imwrite(curr_dest_path, window)
    # plt.imshow(windows1[0],cmap='gray')
    # plt.show()

    orig_x = curr_image_coords['x'].values
    orig_y = curr_image_coords['y'].values
    image_base_name,extention = os.path.splitext(img_name)

    for i, window in enumerate(windows):
        plt.imshow(window,cmap='gray')
        plt.show()
        curr_name = image_base_name + f'_Block_{i}'


        x_on_image_min = (i % B.shape[1]) * step[1]
        y_on_image_min = np.floor(i / B.shape[1]).astype(int) * step[0]

        #get indices of crops in current window
        x_to_change = np.logical_and((x_on_image_min <= orig_x),
                      (orig_x <= (x_on_image_min + window_shape[0])))

        y_to_change = np.logical_and((y_on_image_min <= orig_y),
                                     (orig_y <= y_on_image_min + window_shape[1]))

        to_change_ind = np.where(np.logical_and(x_to_change,y_to_change))
        if len(to_change_ind[0]) >0:
            # get actual coordinates to change
            rows = curr_image_coords.iloc[to_change_ind].copy()
            rows['image_id'] = curr_name
            rows['width'] = window.shape[1]
            rows['height'] = window.shape[0]

            rows['x'] = rows['x'] - x_on_image_min
            rows['y'] = rows['y'] - y_on_image_min

            rows['x'],rows['w'] = zip(*rows.apply(lambda x: check_coord(x['x'],x['w'],x['width']),axis=1))
            rows['y'], rows['h'] = zip(*rows.apply(
                lambda x: check_coord(x['y'], x['h'], x['height']),axis=1))


            ret_df = ret_df.append(rows,ignore_index=True)


        curr_dest_path = os.path.join(dest_path, curr_name + extention)
        cv2.imwrite(curr_dest_path, window)
    # print("d")
    return ret_df


def slice_image_with_padding(img, step_overlap,
                             window_shape):
    step = []
    # step = img.shape/window_shape
    for i in range(len(window_shape)):
        if i < 2:
            step.append(window_shape[i] - step_overlap)
        else:
            step.append(window_shape[i])
    pad_x = get_pad_size(img, window_shape, step, 0)
    pad_y = get_pad_size(img, window_shape, step, 1)
    new_shape = []
    for i in img.shape:
        new_shape.append(i)
    new_shape[0] += pad_x
    new_shape[1] += pad_y
    pad_image = np.zeros(new_shape)
    pad_image[:img.shape[0], :img.shape[1]] = img

    B = view_as_windows(pad_image, window_shape=window_shape, step=step)
    windows = B.reshape(-1, *window_shape)
    return B, step, windows




def csv_to_coco(src_path):
    dest_path,json_name = os.path.split(src_path)
    json_name = os.path.splitext(json_name)[0]
    df = pd.read_csv(src_path)
    coco = {"images": [],
            "categories": [
                {
                    "supercategory": "none",
                    "name": "mosquito",
                    "id": 0
                }
            ],
            "annotations": []
            }

    id =0
    annotation_id = 0
    for group in tqdm(df.groupby("image_id")):
        group_df = group[1]
        width = group_df.iloc[0]['width']
        height = group_df.iloc[0]['height']
        img_dict = {"file_name": group[0],
                    "height": height, "width": width,
                    "id": id}
        coco['images'].append(img_dict)
        id += 1
        for i in range(len(group_df)):
            curr = group_df.iloc[i]

            coco_annotation = {
                "id": annotation_id,
                "bbox": [
                    curr['xmin'],
                    curr['ymin'],
                    curr['w'],
                    curr['h']
                ],
                "image_id": id,
                "segmentation": [],
                "ignore": 0,
                "area": int(curr['w']) * int(curr['h']),
                "iscrowd": 0,
                "category_id": 0
            }
            annotation_id += 1
            coco['annotations'].append(coco_annotation)


    with open(os.path.join(dest_path, json_name + ".json"), 'w') as f:
        json.dump(coco, f)






### crop ####

def init_crop(area, continue_last, dest_path, gray, iou_thr, permit_range, size):
    crops_alg = mosquito_blob_detection(roi_size=size,
                                       gray_thr=gray,
                                       iou_thr=iou_thr,
                                       blob_min_area=area,
                                       permit_range=permit_range)  # ,gray_thr=62)
    if not continue_last:
        start_time = datetime.datetime.now()
        current_time = start_time.strftime("%d_%m_%y__%H_%M_%S")
        dest_path = os.path.join(dest_path, current_time)

        createFolder(dest_path)
    return dest_path, crops_alg

def get_crops(cimg, detection_alg, visualize):

    #dims = len(np.shape(cimg))
    detections, all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec, clean_img = \
        detection_alg.extract_blobs_from_img(cimg, show_results=visualize, reduce_duplicates=True)
    return detections, all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec, clean_img

def is_valid_img_pixel(img_shape, x0, y0, xe, ye):
    x0 = max(x0, 0)
    y0 = max(y0, 0)
    xe = min(xe, img_shape[1] - 1)
    ye = min(ye, img_shape[0] - 1)
    return x0, y0, xe, ye

def crop_imprint_img(img_gray,bbx_vec, all_w_h_vec, all_diag_vec, size):
    mbr_mat = []
    for i, det in enumerate(bbx_vec):
        wh = all_w_h_vec[i]
        cmax_size = min(max(size, int(all_diag_vec[i] + 0.5), int(wh[0] + 0.5), int(wh[1] + 0.5)), int(1.12 * size))
        mn_lenx = min(0.1 * size, 10)
        mn_leny = min(0.1 * size, 10)
        x1 = int(det[0] - mn_lenx - 0.5)
        y1 = int(det[1] - mn_leny - 0.5)
        x2 = int(x1 + cmax_size + mn_lenx + 0.5)
        y2 = int(y1 + cmax_size + mn_leny + 0.5)
        x1, y1, x2, y2 = is_valid_img_pixel(img_gray.shape, x1, y1, x2, y2)
        p1 = (x1, y1)
        p2 = (x2, y2)
        cv2.rectangle(img_gray, p1, p2, (0, 0, 255), 10)
        mbr_vec = (x1, y1, x2, y2)
        mbr_mat += [mbr_vec]
    return img_gray, mbr_mat


def center_crop_imprint_img(img_gray, bbx_vec, all_w_h_vec, all_diag_vec, all_centers_vec, size):
    mbr_mat = []
    for i, det in enumerate(bbx_vec):
        wh = all_w_h_vec[i]
        cx_cy = all_centers_vec[i]
        cmax_size = min(max(size, int(all_diag_vec[i] + 0.5), int(wh[0] + 0.5), int(wh[1] + 0.5)), int(1.12 * size))
        shft = int(1.2 * size)
        xy0 = (cx_cy[0] - 0.5 * cmax_size, cx_cy[1] - 0.5 * cmax_size)
        xye = (cx_cy[0] + 0.5 * cmax_size, cx_cy[1] + 0.5 * cmax_size)
        mn_lenx = min(0.1 * size, 10)
        mn_leny = min(0.1 * size, 10)
        # if preds is None or preds[i] != 'discarded':
        x1 = int(min(det[0], xy0[0]) - mn_lenx - 0.5)
        y1 = int(min(det[1], xy0[1]) - mn_leny - 0.5)
        x2 = int(max(x1 + cmax_size, xye[0]) + mn_lenx + 0.5)
        y2 = int(max(y1 + cmax_size, xye[1]) + mn_leny + 0.5)
        x1, y1, x2, y2 = is_valid_img_pixel(img_gray.shape, x1, y1, x2, y2)
        p1 = (x1, y1)
        p2 = (x2, y2)
        cv2.rectangle(img_gray, p1, p2, (0, 0, 255), 10)
        mbr_vec = (x1, y1, x2, y2)
        mbr_mat += [mbr_vec]
    return img_gray, mbr_mat



def get_mbrs_data(large_cc, recs, mbr_vec, int_mbr_vec, img=None, clean_img=None, ext=True):
    def dist_points(p1, p2):
        if not isinstance(p1, np.ndarray):
            p1 = np.array(p1)
        if not isinstance(p2, np.ndarray):
            p2 = np.array(p2)
        # finding sum of squares
        sum_sq = np.sum(np.square(p1 - p2))
        return np.sqrt(sum_sq)  # np.sqrt(p0[0]*p1[0]+p0[1]*p1[1])

    all_mbr_vec = []
    all_diag_vec = []
    all_centers_vec = []  # cx,cy
    all_w_h_vec = []
    for ii in range(len(large_cc)):
        clnt = large_cc[ii]
        # x, y, w, h = cv2.boundingRect(cnt)
        clrec = recs[ii]
        # cv2.minAreaRect(cnt) => [cx, cy, w, h, theta] =>
        # cx, cy = cmbr[0] ,  w, h = cmbr[1], theta = cmbr[2] degrees
        cmbr = mbr_vec[ii]
        # Gets center of rotated rectangle
        all_centers_vec += [cmbr[0]]
        # Gets width (cmbr[1][0]) and height (cmbr[1][1]) of rotated rectangle
        all_w_h_vec += [cmbr[1]]
        # cbox = box_mbr_vec[ii]
        cbox_int = int_mbr_vec[ii]
        print('ii, clrec, cmbr, cbox_int = ', ii, clrec, cmbr[:2], cbox_int)
        mnx = min(cbox_int[:, 0])
        mxx = max(cbox_int[:, 0])
        mny = min(cbox_int[:, 1])
        mxy = max(cbox_int[:, 1])
        p1 = (mnx, mny)
        p2 = (mxx, mxy)
        cdiag_dist = dist_points(p1, p2)
        if ext:
            x0 = min(clrec[0], mnx)
            w0 = mxx-mnx
            y0 = min(clrec[1], mny)
            h0 = mxy-mny
            w = max(clrec[2], w0)
            h = max(clrec[3], h0)
            xe = x0 + w
            ye = y0 + h
        else:
            x0 = clrec[0]
            y0 = clrec[1]
            w = clrec[2]
            h = clrec[3]
            xe = x0 + w
            ye = y0 + h

        if img is not None:
            x0, y0, xe, ye = is_valid_img_pixel(img.shape, x0, y0, xe, ye)
        p1 = (x0, y0)
        p2 = (xe, ye)
        crect_dist = dist_points(p1, p2)
        print('cdiag_dist = ', cdiag_dist, 'crect_dist = ', crect_dist)
        all_diag_vec.append(max(cdiag_dist, crect_dist, cmbr[1][0], cmbr[1][1]))
        # lcimg = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
        if img is not None  and clean_img is not None:
            lcimg = img[y0:ye, x0:xe]
            cshape = (lcimg.shape[0], lcimg.shape[1], 3)
            shft = np.array([x0, y0], dtype=np.int32)
            nclnt = clnt - shft
            img_contours_diff = get_image_contours(cshape, nclnt, fill_cont=False)
            chull_diff, chull, binary = get_convexHull_img(img_contours_diff)
            # img_name = cmethod + '_convexHull_img_'+ str(ii) + '_'+ str(h) + '_'+ str(w) + '.jpg'
            # isWritten = cv2.imwrite(os.path.join(output_dir, img_name), 255*chull.astype('uint8'))
            # if not isWritten:
            #    print('Error: The image is not saved. chull.shape = ', chull_diff.shape)
            # img_name = cmethod + '_diff_convexHull_img_'+ str(ii) + '_'+ str(h) + '_'+ str(w) + '.jpg'
            # cfile = os.path.join(output_dir, img_name)
            # if not os.path.isfile(cfile):
            # cval_max = np.max(chull_diff)
            # cval_min = np.min(chull_diff)
            # chull_diff = np.int32(255 * (chull_diff - cval_min) / (cval_max - cval_min) + 0.5)
            # isWritten = cv2.imwrite(cfile, chull_diff.astype('uint8'))
            # if not isWritten:
            #    print('Error: The image is not saved. chull_diff.shape = ', chull_diff.shape)

            blob_img, minmax_vec = get_blob_img(clean_img, chull, binary, shft)
            # minmax_vec = (rmin, rmax, cmin, cmax)
            all_mbr_vec += [minmax_vec]
            # img_name = cmethod + '_blob_img_'+ str(ii) + '_'+ str(h) + '_'+ str(w) + '.jpg'
            # cfile = os.path.join(output_dir, img_name)
            # if not os.path.isfile(cfile):
            # isWritten = cv2.imwrite(cfile, blob_img)
            # if not isWritten:
            #    print('Error: The image is not saved. blob_img.shape = ', blob_img.shape)
            # print(' all_diag_vec = ', sorted(all_diag_vec)) #all_diag_vec =  [109.98636279102969, 278.6897917039661, 324.1049212832166, 488.0430308896952]
    return all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec


def run_crop_on_image(src_name, dest_path,
                                 size=250,gray=20,iou_thr=0.3,area=700,
                                 visualize=False, permit_range=[],
                                 continue_last = False):
    global output_dir
    #src_path = dest_path
    dest_path, crops_alg = init_crop(area, continue_last, dest_path, gray, iou_thr, permit_range, size)
    output_dir = dest_path


#    for id,im_path in enumerate(sorted(glob.glob(os.path.join(src_path,'*')))):
    name = os.path.basename(src_name)
    dest = os.path.join(dest_path, name)
    if os.path.exists(dest):
        print(f"skipping {name}")
        #continue
    else:
        print(f"working on {name}")

    img_BGR = cv2.imread(src_name) #bgr
    if img_BGR is not None:
        dims = len(img_BGR.shape)
        if dims > 2:
            is_same = np.array_equal(img_BGR[:,:,0], img_BGR[:,:,1]) and np.array_equal(img_BGR[:,:,2], img_BGR[:,:,1])
            if is_same:
                img_gray0 = img_BGR[:,:,0]
                img_gray = cv2.cvtColor(img_BGR, cv2.COLOR_BGR2GRAY)
                print( 'img_gray', np.array_equal(img_gray0, img_gray) )
            else:
                img_gray = cv2.cvtColor(img_BGR, cv2.COLOR_BGR2GRAY)
        else:
            img_gray = img_BGR

        run_clean = False
        if run_clean:
            img_gray = run_clean_img_rectangle_slices(img_gray, y_slicesize=size, x_slicesize=size)
        bbx_vec, all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec, clean_img = get_crops(img_gray, crops_alg, visualize)


        #  not working      img_gray_lighcomp = lighcomp(img_gray)
#        lighcomp_dest = os.path.join(dest_path, 'lighcomp_gray.jpg')
#        isWritten = cv2.imwrite(lighcomp_dest, img_gray_lighcomp)
#        if not isWritten:
#            print('Error: The image is not saved. img_gray_lighcomp.shape = ', np.shape(img_gray_lighcomp))
        test_img_gray = img_gray.copy()
        test_img_gray0 = img_gray.copy()
        img_gray, mbr_mat = crop_imprint_img(img_gray, bbx_vec, all_w_h_vec, all_diag_vec,  size)


        gray_crop_dest_dir = debug_detect_crop(test_img_gray, mbr_mat, output_dir, pref='gray_')
        clean_crop_dest_dir = debug_detect_crop(clean_img, mbr_mat, output_dir, pref='cl0_')
        dest0 = os.path.join(gray_crop_dest_dir, name)
        isWritten = cv2.imwrite(dest0, img_gray)
        if not isWritten:
            print('Error: The image is not saved. img_gray.shape = ', np.shape(img_gray))
        #cl_dest = os.path.join(clean_crop_dest_dir, 'cl_gray0.jpg')
        #cl_out = run_clean_img_rectangle_slices(test_img_gray, y_slicesize=size, x_slicesize=size)
        #isWritten = cv2.imwrite(cl_dest, cl_out)
        #if not isWritten:
        #    print('Error: The image is not saved. img_gray.shape = ', np.shape(img_gray))
        clean_out2 = run_clean_img_rectangle_slices(clean_img, y_slicesize=size, x_slicesize=size)
        cl_dest2 = os.path.join(dest_path, 'cl_clean2.jpg')
        isWritten = cv2.imwrite(cl_dest2, clean_out2)
        if not isWritten:
            print('Error: The image is not saved. img_gray.shape = ', np.shape(img_gray))

        debug_rerunCrop(clean_out2, crops_alg, clean_crop_dest_dir, visualize, size)
        #clean_img
        clean_img0 = clean_img.copy()
        clean_img1 = clean_img.copy()
        clean_img, _ = crop_imprint_img(clean_img, bbx_vec, all_w_h_vec, all_diag_vec, size)

        dest0 = os.path.join(clean_crop_dest_dir, 'test_clean_img0.jpg')
        isWritten = cv2.imwrite(dest0, clean_img)
        if not isWritten:
            print('Error: The image is not saved. clean_img.shape = ', np.shape(clean_img))

        test_img_gray, mbr_mat = center_crop_imprint_img(test_img_gray, bbx_vec, all_w_h_vec, all_diag_vec, all_centers_vec, size)


            #cv2.imwrite(dest, img_RGB)
        gray0_crop_dest_dir = debug_detect_crop(test_img_gray0, mbr_mat, output_dir, pref='gray0_')
        dest0 = os.path.join(gray0_crop_dest_dir, 'result_centmbr.jpg')
        isWritten = cv2.imwrite(dest0, test_img_gray)
        if not isWritten:
            print('Error: The image is not saved. test_img_gray.shape = ', np.shape(test_img_gray))

        #clean_img
        clean_img0, mbr_mat = center_crop_imprint_img(clean_img0, bbx_vec, all_w_h_vec, all_diag_vec, all_centers_vec, size)
        cleanCent_crop_dest_dir = debug_detect_crop(clean_img1, mbr_mat, output_dir, pref='cleanCent_')
        dest0 = os.path.join(cleanCent_crop_dest_dir, 'result_clean_img_cmbr.jpg')
        isWritten = cv2.imwrite(dest0, clean_img0)
        if not isWritten:
            print('Error: The image is not saved. clean_img.shape = ', np.shape(clean_img))


def debug_rerunCrop(cimg, detection_alg, dest_path, visualize, size=250):

    dims = len(np.shape(cimg))
    bbx_vec, all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec, clean_img = \
        detection_alg.extract_blobs_from_img(cimg, show_results=visualize, reduce_duplicates=True)
    #preds = None
    #return detections, preds, all_mbr_vec, all_diag_vec, all_centers_vec, all_w_h_vec, clean_img
    test_img_gray = clean_img.copy()
    test_img_gray0 = clean_img.copy()
    clean_img, mbr_mat = crop_imprint_img(clean_img, bbx_vec, all_w_h_vec, all_diag_vec, size)

    cl_dest4 = os.path.join(dest_path, 'cl_clean4.jpg')
    isWritten = cv2.imwrite(cl_dest4, clean_img)
    if not isWritten:
        print('Error: The image is not saved. clean_img.shape = ', np.shape(clean_img))
    cl4_crop_dest_dir = debug_detect_crop(test_img_gray, mbr_mat, output_dir, pref='cl4_')

    test_img_gray0, mbr_mat = center_crop_imprint_img(test_img_gray0, bbx_vec, all_w_h_vec, all_diag_vec, all_centers_vec, size)

    cl_dest4 = os.path.join(cl4_crop_dest_dir, 'cl_clean44.jpg')
    isWritten = cv2.imwrite(cl_dest4, test_img_gray0)
    if not isWritten:
        print('Error: The image is not saved. test_img_gray0.shape = ', np.shape(test_img_gray0))
    #debug_detect_crop(test_img_gray0, mbr_mat, output_dir, pref='cl44_')






def main():
    # dest_path = r'D:\Files\Projects\Senecio\Development\Sorter-Machine\mosquito-detection\data\experiments\output_images\ALL'
     # src_path = r'D:\Files\Projects\Senecio\Development\Sorter-Machine\mosquito-detection\data\experiments\input_images\src_images\test_vis'
    #src_path = r'D:\Files\Projects\Senecio\Development\Sorter-Machine\mosquito-detection\data\experiments\input_images\src_images\04_10_19'
    #--- kaggle paths ----

    # src_path = r'D:\Files\Projects\Senecio\Development\Sorter-Machine\mosquito-detection\data\experiments\input_images\src_images\All1'
    # dest_path = r'D:\Files\Projects\Senecio\Development\Sorter-Machine\mosquito-detection\data\experiments\output_images\ALL1'

    #src_path = r'C:\Users\gabiw\Senecio_robotics\Data\full_image'
    #src_path = base_data_dir + 'full_image'
    #dest_path = base_data_dir + 'full_image/out'
    # src_path = r'C:\Users\gabiw\Senecio_robotics\Data\full_image'
    #src_path = r'C:\Users\gabiw\Senecio_robotics\Data\full_image'
    base_dir = 'C:\\Users\\gabiw\\Senecio_robotics\\Data\\full_image\\cluster_robot_dataset\\'
    src_path = base_dir + 'normal\\'
    # src_path = base_data_dir + 'full_image'
    #dest_path = r'C:\Users\gabiw\Senecio_robotics\Data\full_image\out'
    dest_path = src_path + 'out\\'
    # dest_path = base_data_dir + 'full_image/out'

    test_img = True
    run_on_dir = False
    yen_run_on_dir = False

    # permit_range = []
    # permit_range = [50.0, 3996.0, -100.0, 3100.0] # important! otherwise the
    # mosquitos at the borders are labeled
    permit_range = [50.0, 3896.0, -100.0, 3100.0]
    # permit_range = [100.0, 3896.0, -100.0, 3100.0]

    if test_img:
        import os
        #src_img = '01_02_20__00_12_14_start__cycle_260 _Not_Empty.png'
        #src_img = 'florida19_7_Camera0_Light100_non-empty_M16_04_09_19__17_12_44.png'
        #src_img = 'florida19_826_Camera0_Light100_non-empty_M2210_29_08_19__00_12_35.png'
        #src_img = 'florida19_826_Camera0_Light100_non-empty_M2210_29_08_19__00_12_35.png'
        #src_img = 'florida19_4_Camera0_Light100_non-empty_M2_23_10_19__16_09_04.jpg'
        src_img = '14_01_20__23_06_56_Full_cycle_33__start_first_before_classify_left.jpg'
        src_name = os.path.join(src_path, src_img)
        run_crop_on_image(src_name, dest_path, size=250, gray=20,iou_thr=0.3,area=700, visualize=False,
                                     permit_range=permit_range, continue_last=False)

    elif yen_run_on_dir:
        import os, cv2
        dest_path = init_yen(dest_path)

        src_files_img_vec, src_fullname_img_lists = get_path_images(src_path)
        print('len(src_fullname_img_lists) = ', len(src_fullname_img_lists))
        n_iter = 1
        for id, src_name in enumerate(sorted(src_fullname_img_lists)):
            print('id, src_name ', id, src_name)
            name = os.path.basename(src_name)
            dst_name = 'clean_' + str(n_iter) + '_' + str(id) + '_' + name
            dest = os.path.join(dest_path, dst_name)
            if os.path.exists(dest):
                print(f"skipping {name}")
                continue
            else:
                print(f"working on {name}")
            #run_yen_on_image(src_name, n_iter=1, y_slicesize=112, x_slicesize=112, ovFac=2):
            clean_img = run_yen_on_image(src_name, n_iter=n_iter)


            dest = os.path.join(dest_path, dst_name)
            #print('debug: The image is not saved. id, clean_img.shape = ', id, dest, np.shape(clean_img))
            isWritten = cv2.imwrite(dest, clean_img)
            if not isWritten:
                print('Error: The image is not saved. id, clean_img.shape = ', id, np.shape(clean_img))

    elif run_on_dir:

        src_files_img_vec, src_fullname_img_lists = get_path_images(src_path)
        print('len(src_fullname_img_lists) = ', len(src_fullname_img_lists))

        for id, src_name in enumerate(sorted(src_fullname_img_lists)):
            print('id, src_name ', id, src_name)
            run_crop_on_image(src_name, dest_path, size=250, gray=20, iou_thr=0.3, area=700, visualize=False,
                                   permit_range=permit_range, continue_last=False)

"""    else:
        run_detection_on_src_folder(src_path,
                                     dest_path,
                                     size=250,gray=20,iou_thr=0.3,area=700,
                                     visualize=False,
                                     use_discarded_model=False,
                                     permit_range=permit_range,
                                     continue_last=False)
"""

    # --- use of blocking method- create block of images
    # window_size = (1200, 1600)
    # window_overlap = 250
    # create_blocks_from_src_images(src_path, dest_path, window_size, window_overlap)


    # --- create kagle dataset
    # create_kaggle_data_set(src_path,dest_path,
    #                              size=270,gray=20,iou_thr=0.3,area=700,
    #                              permit_range=permit_range,
    #                        test_size=0)
    # slicing kaggle dataset
    # df_path = r"D:\Files\Projects\Senecio\Development\Sorter-Machine\mosquito-detection\data\experiments\output_images\kaggle_template\05_10_20__16_20_33\train.csv"
    # src_path = r"D:\Files\Projects\Senecio\Development\Sorter-Machine\mosquito-detection\data\experiments\output_images\kaggle_template\05_10_20__16_20_33\train"
    # dest_path = r"D:\Files\Projects\Senecio\Development\Sorter-Machine\mosquito-detection\data\experiments\output_images\slicing-draw"
    # slice_dataset(df_path,src_path,dest_path,window_size,window_overlap,visualize=True)
    #

    # use method to convert csv to -coco format
    # csv_to_coco(r"D:\Files\Projects\Senecio\Development\Sorter-Machine\facebook_detr\data\Florida_data\test.csv")

    # testing code for pathc detect
    # use_patches_detect(src_path,dest_path,
    #                              size=250,gray=20,iou_thr=0.3,area=700,
    #                              permit_range=permit_range)


    # ---------- testing visualiztion method to make sure the annotation file is correct
    # in the formed dataset
    # src_path =r'D:\Files\Projects\Senecio\Development\Sorter-Machine\mosquito-detection\data\experiments\output_images\kaggle_template\05_10_20__14_28_21\train'
    # df_path =r"D:\Files\Projects\Senecio\Development\Sorter-Machine\mosquito-detection\data\experiments\output_images\kaggle_template\05_10_20__14_28_21\train.csv"
    # visualize_kaggle_data_set(df_path, src_path, dest_path)

if __name__ == "__main__":
   main()
