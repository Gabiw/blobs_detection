"""
filter_image_utils.py
start version  :29/11/20
Created on 29/11/20
@author: Gabriel Weizman
"""
import numpy as np
import cv2
DEFAULT_IMAGE_SIZE = 224
EPS_SAF = 2.22e-16

from PIL import Image
from PIL import ImageFile
#ImageFile.LOAD_TRUNCATED_IMAGES = True
from skimage.filters import threshold_yen
from skimage.filters import unsharp_mask
from skimage.filters import threshold_multiotsu
from skimage.morphology import convex_hull_image
from skimage import util
from skimage import img_as_float
from skimage.util import invert
from skimage.filters import laplace
from skimage.exposure import match_histograms

import os, datetime
from os import listdir, path, makedirs, scandir, remove, unlink, rmdir
from shutil import copyfile, move
import os, errno
from os import listdir, path, makedirs, scandir, remove, unlink, rmdir

def get_path_images(c_dir, show=False):
    def __get_images(dir):
        ext = ['.jpg', '.png', '.jpeg', '.gif', '.jpg!s']
        return [file for file in os.listdir(dir) if
                os.path.isfile(os.path.join(dir, file)) and file[file.rfind('.'):].lower() in ext]

    files_img_vec = []
    all_img_lists = []
    if show:
        print('\n c_dir  = ', c_dir)
    isexist = path.exists(c_dir)
    if not isexist:
        print(c_dir, 'not exists')
    else:
        if show:
            print(c_dir, 'ok exists')
        c_img_vec = __get_images(c_dir)
        files_img_vec.extend(c_img_vec)
        all_img_lists.extend([c_dir + f for f in c_img_vec])
        print(' len(files_img_vec), len(all_img_lists) = ', len(files_img_vec), len(all_img_lists))
        if len(files_img_vec) != len(all_img_lists):
            print(' error c_dir, len(files_img_vec) != len(all_img_lists): ', c_dir, len(files_img_vec),
                  len(all_img_lists))

    return files_img_vec, all_img_lists

def createFolder(dir):
    try:
        isexist = os.path.exists(dir)
        if not isexist:
            os.makedirs(dir)
    except Exception as inst:
        print("createFolder had exception on : ", inst)

def init_yen(dest_path):
    start_time = datetime.datetime.now()
    current_time = start_time.strftime("%d_%m_%y__%H_%M_%S")
    dest_path = os.path.join(dest_path, current_time)
    createFolder(dest_path)
    return dest_path

def get_COLOR_BGR2RGB(img):
    try:
        img_out = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    except Exception as inst:
        LOG.warn("cv2.cvtColor(img0, cv2.COLOR_BGR2RGB) had exception on get_COLOR_BGR2RGB: %s" % str(inst))
        img_out = None
        pass
    return img_out


def loc_resize(img, out_size):
    #out_size = (int(img.shape[1]), int(img.shape[0]))  # (width, height)
    return cv2.resize(img, out_size, interpolation=cv2.INTER_AREA)

def fix_quart_blk(img_center, img_c, img_blk):
    out_size = (img_center.shape[1], img_center.shape[0])  # (width, height) <=> (x, y)
    #img_c = img[:r1, :c1]
    img_c = loc_resize(img_c, out_size)
    #img_blk = img_out[:r1, :c1]
    f_size = (img_blk.shape[1], img_blk.shape[0])
    tmp_img = match_histograms(img_c, img_center, multichannel=True)
    out_blk = loc_resize(tmp_img, f_size)
    return out_blk

def lighcomp(img):

    img_out = img.copy()
    dims = img.shape[:2]
    rows = dims[0]
    cols = dims[0]
    r1 = int(rows/3-0.5)
    r2 = int(2*rows/3+0.5)
    c1 = int(cols/3-0.5)
    c2 = int(2*cols/3+0.5)
    img_center = img[r1:r2, c1:c2]
    #out_size = (img_center.shape[1], img_center.shape[0]) # (width, height) <=> (x, y)
    img_c = img[:r1, :c1]
    #img_c = loc_resize(img_c, out_size)
    img_blk = img_out[:r1, :c1]
    img_out[:r1, :c1] = fix_quart_blk(img_center, img_c, img_blk)
    #f_size = (img_blk.shape[1], img_blk.shape[0])
    #tmp_img = match_histograms(img_c, img_center, multichannel=True)
    #img_out[:r1, :c1] = loc_resize(tmp_img, f_size)

    img_c = img[:r1, c1:c2]
    img_blk = img_out[:r1, c1:c2]
    img_out[:r1, c1:c2]  = fix_quart_blk(img_center, img_c, img_blk)


    img_c = img[:r1, c2:]
    img_blk = img_out[:r1, c2:]
    img_out[:r1, c2:] = fix_quart_blk(img_center, img_c, img_blk)

    img_c = img[r1:r2, :c1]
    img_blk = img_out[r1:r2, :c1]
    img_out[r1:r2, :c1] = fix_quart_blk(img_center, img_c, img_blk)

    img_c = img[r1:r2, c2:]
    img_blk = img_out[r1:r2, c2:]
    img_out[r1:r2, c2:] = fix_quart_blk(img_center, img_c, img_blk)

    img_c = img[r2:, :c1]
    img_blk = img_out[r2:, :c1]
    img_out[r2:, :c1] = fix_quart_blk(img_center, img_c, img_blk)

    img_c = img[r2:, c1:c2]
    img_blk = img_out[r2:, c1:c2]
    img_out[r2:, c1:c2] = fix_quart_blk(img_center, img_c, img_blk)

    img_c = img[r2:, c2:]
    img_blk = img_out[r2:, c2:]
    img_out[r2:, c2:] = fix_quart_blk(img_center, img_c, img_blk)

    return img_out

def filter_otsu(gray_img, num_classes=3):
    # gray_thr = threshold_multiotsu(img, classes=2, nbins=256)
    # Using the threshold values, we generate the three regions.
    # regions = np.digitize(img, bins=gray_thr)
    # img_out = regions.astype('uint8')
    # Applying multi-Otsu threshold for the default value, generating
    # three classes.
    # threshold_multiotsu(image, classes=3, nbins=256)
    gray_thr_vec = threshold_multiotsu(gray_img, classes=num_classes)
    #gray_thr = gray_thr_vec[0]
    # Using the threshold values, we generate the three regions.
    regions = np.digitize(gray_img, bins=gray_thr_vec)
    # set(regions.flatten())) => {0, 1, 2} 0= black-object , 1 = mid-checker,  2 = net-white lines
    val_vec = tuple(set(regions.flatten()))
    hval = max(val_vec[1], 1)
    img_out = (regions < hval).astype('uint8')
    return img_out


# img_out = util.invert(bin_img) * gray_img
# img_out = util.invert(bin_img) * gray_img
# gray_thr, img_out = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)


def filetr_yen(gray_img):
    # from skimage import util
    yen_gray_thr = threshold_yen(gray_img)
    img_out = (gray_img < yen_gray_thr).astype('uint8')
    return img_out

def yen_filter_patch(cimg):
    #cmethod = 'yen'  # 'otsu_small_blobs' #'fix'
    bin_img = filetr_yen(cimg)
    clean_img = util.invert(bin_img * util.invert(bin_img * cimg))
    return clean_img

def filter_patch(cimg, method="yen"): #debug_filter_patch
    if method == "yen":
        bin_img = filetr_yen(cimg)
    elif method == "otsu":
        bin_img = filter_otsu(cimg)
    else:
        bin_img = filetr_yen(cimg)
    clean_img = util.invert(bin_img * util.invert(bin_img * cimg))
    return clean_img


def run_clean_img_rectangle_slices(img, y_slicesize=DEFAULT_IMAGE_SIZE, x_slicesize=DEFAULT_IMAGE_SIZE, ovFac=2, method="yen"):
    img_out = img.copy()
    width = img.shape[1]
    height = img.shape[0]
    if img_out.shape[0] <= DEFAULT_IMAGE_SIZE and img_out.shape[1] <= DEFAULT_IMAGE_SIZE:
        return filter_patch(img_out, method=method)

    if height<=y_slicesize and width<=x_slicesize:
        print('warning run_clean_img_rectangle_slices: image size is smaller than slice size = ', img.shape, y_slicesize, x_slicesize)
        return img_out
    y_end = height - y_slicesize
    x_end = width - x_slicesize
    y_starts = list(range(0, y_end + 1, y_slicesize//ovFac))
    if len(y_starts) > 0 and y_starts[len(y_starts)-1] < y_end - 1:
        y_starts.append(y_end)
    x_starts = list(range(0, x_end + 1, x_slicesize//ovFac))
    if len(x_starts) > 0 and x_starts[len(x_starts)-1] < x_end - 1:
        x_starts.append(x_end)
    #slices_image = [img[y:y + y_slicesize, x:x + x_slicesize] for y in y_starts for x in x_starts]
    for y in y_starts:
        ye = y + y_slicesize
        for x in x_starts:
            xe = x + x_slicesize
            cimg = img[y:ye, x:xe]
            img_out[y:ye, x:xe] = filter_patch(cimg, method=method)
    return img_out


def clean_filter_image(gray_img, n_iter=1, method="yen"):
    img_out = gray_img.copy()
    dims = len(img_out.shape)
    if dims > 2:
        img_out = np.squeeze(img_out)
    t_counter = 0
    for index in range(n_iter):
        img_out = run_clean_img_rectangle_slices(img_out, method=method)
        img_out = img_out.transpose()
        t_counter += 1

        # print('debug0: src_name, img_out.shape = ', index, img_out.shape)
    if t_counter % 2 > 0:
        img_out = img_out.transpose()
    return img_out


def run_yen_on_image(img, n_iter=1, method="yen"):
    img_out = img.copy()
    if img is not None:
        dims = len(img.shape)
        if dims > 2:
            for index in range(dims):
                img_out[:,:,index] = clean_filter_image(img[:,:,index], n_iter=n_iter, method=method)
        else:
            #img_gray = img
            img_out = clean_filter_image(img, n_iter=n_iter, method=method)
        #print('debug0: src_name, img_out.shape = ', src_name, img_gray.shape)
    return img_out


def superimpose_crop_patch(cimg, roi, patch_img): #debug_add_crop_patch
    h, w = cimg.shape[:2]
    sh, sw = patch_img.shape[:2]
    w0 = max(roi[0], 0)
    we = min(roi[0] + sw, w-1)
    h0 = max(roi[1], 0)
    he = min(roi[1] + sh, h-1)
    cimg[h0:he, w0:we] = patch_img
    return cimg

def get_crop_image(cimg, mbr_vec): #debug_crop_detection
    h, w = cimg.shape[:2]
    return cimg[max(mbr_vec[1], 0):min(mbr_vec[3], h-1), max(mbr_vec[0], 0):min(mbr_vec[2], w-1)]

def superimpose_all_crops(img, mbr_mat, method="yen"):# = (x1, y1, x2, y2)): debug_detect_crop
    img_out = img.copy()
    #dims = len(np.shape(cimg))
    #start_time = datetime.datetime.now()
    #current_time = start_time.strftime("%d_%m_%y__%H_%M_%S")
    #crop_dest_dir = output_dir + '\\crops'
    #createFolder(crop_dest_dir)
    for ind, mbr_vec in enumerate(mbr_mat):
        tmp = get_crop_image(img, mbr_vec)
        #name = pref + 'crop_'+ str(ind) + '.jpg'
        #crop_dest = os.path.join(crop_dest_dir, name)
        #debug_save_crop(tmp, crop_dest)
        clean_patch = filter_patch(tmp, method=method)
        #name = pref + 'clean_patch_'+ str(ind) + '.jpg'
        #crop_dest = os.path.join(crop_dest_dir, name)
        #debug_save_crop(clean_patch, crop_dest)
        img_out = superimpose_crop_patch(img_out, mbr_vec[:2], clean_patch)
    #name = pref + 'img_clean' + '.jpg'
    #crop_dest = os.path.join(crop_dest_dir, name)
    #debug_save_crop(img_out, crop_dest)
    return img_out


def debug_save_crop(cimg, fullname):
    if not os.path.isfile(fullname):
        if len(np.shape(cimg)) < 3:
            rgb_img = cv2.cvtColor(cimg.astype('uint8'), cv2.COLOR_GRAY2RGB)
        else:
            rgb_img = cimg
        isWritten = cv2.imwrite(fullname, rgb_img)
        if not isWritten:
            print('debug_save_crop Error: The crop_dest is not saved. image.shape = ', np.shape(rgb_img))
    else:
        print('debug_save_crop warning: The crop_dest is not saved. already exists ')


def debug_detect_crop(cimg, mbr_mat, output_dir, pref='_', method="yen"):# = (x1, y1, x2, y2)):
    img_out = cimg.copy()
    #dims = len(np.shape(cimg))
    #start_time = datetime.datetime.now()
    #current_time = start_time.strftime("%d_%m_%y__%H_%M_%S")
    crop_dest_dir = output_dir + '\\crops_'+ pref
    createFolder(crop_dest_dir)
    for ind, mbr_vec in enumerate(mbr_mat):
        tmp = get_crop_image(cimg, mbr_vec)
        name = pref + 'crop_'+ str(ind) + '.jpg'
        crop_dest = os.path.join(crop_dest_dir, name)
        debug_save_crop(tmp, crop_dest)
        #clean_patch = debug_filter_patch(tmp, detection_alg)
        clean_patch = filter_patch(tmp, method=method)
        name = pref + 'clean_patch_'+ str(ind) + '.jpg'
        crop_dest = os.path.join(crop_dest_dir, name)
        debug_save_crop(clean_patch, crop_dest)
        img_out = superimpose_crop_patch(img_out, mbr_vec[:2], clean_patch)
    name = pref + 'img_clean' + '.jpg'
    crop_dest = os.path.join(crop_dest_dir, name)
    debug_save_crop(img_out, crop_dest)
    crop_dest_dir = crop_dest_dir + "\\"
    return crop_dest_dir

def image_bin_adjust_thershold(img, gray_thr, method='fix'):
    #saf = gray_thr
    gray_img = img.copy()
    dims = len(img.shape)
    if dims > 2:
        # gray_img = numpy.mean(img, axis=2)
        gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        #img_out = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        #img_out = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        print('adjust_thershold: warning input image is gray !')
        #img_out = (gray < gray_thr).astype('uint8')
        #return img_out
    if method == 'yen':
        from skimage.filters import threshold_yen
        #from skimage import util
        gray_thr = threshold_yen(gray_img)
        img_out = (gray_img < gray_thr).astype('uint8')

    elif method == 'otsu_small_blobs':
        #gray_thr = threshold_multiotsu(img, classes=2, nbins=256)
        # Using the threshold values, we generate the three regions.
        #regions = np.digitize(img, bins=gray_thr)
        #img_out = regions.astype('uint8')
        # Applying multi-Otsu threshold for the default value, generating
        # three classes.
        # threshold_multiotsu(image, classes=3, nbins=256)
        num_classes = 3  # try 5
        gray_thr_vec = threshold_multiotsu(gray_img, classes=num_classes)
        gray_thr = gray_thr_vec[0]
        # Using the threshold values, we generate the three regions.
        regions = np.digitize(gray_img, bins=gray_thr_vec)
        #set(regions.flatten())) => {0, 1, 2} 0= black-object , 1 = mid-checker,  2 = net-white lines
        val_vec = tuple(set(regions.flatten()))
        hval = max(val_vec[1], 1)
        img_out = (regions < hval).astype('uint8')
        #img_out = util.invert(bin_img) * gray_img
        #img_out = util.invert(bin_img) * gray_img
        #gray_thr, img_out = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    elif method == 'otsu_large_blobs':
        #gray_thr = threshold_multiotsu(img, classes=2, nbins=256)
        # Using the threshold values, we generate the three regions.
        #regions = np.digitize(img, bins=gray_thr)
        #img_out = regions.astype('uint8')
        # Applying multi-Otsu threshold for the default value, generating
        # three classes.
        # threshold_multiotsu(image, classes=3, nbins=256)
        num_classes = 3 # try 5
        gray_thr_vec = threshold_multiotsu(gray_img, classes=num_classes)
        gray_thr = gray_thr_vec[0]
        # Using the threshold values, we generate the three regions.
        regions = np.digitize(gray_img, bins=gray_thr_vec)
        #set(regions.flatten())) => {0, 1, 2} 0= black-object , 1 = mid-checker,  2 = net-white lines
        val_vec = tuple(set(regions.flatten()))
        hval = max(val_vec[2], 2)
        img_out = (regions < hval).astype('uint8')
        #img_out = util.invert(bin_img) * gray_img
        #img_out = util.invert(bin_img) * gray_img
        #gray_thr, img_out = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    elif method == 'adaptive_gaussian':
        blockSize = 11
        shift = 0
        img_bin = cv2.adaptiveThreshold(gray_img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, blockSize, shift)
        hval = min(max(set(img_bin.flatten())), 255)
        img_out = ((hval-img_bin)/hval).astype('uint8')
    elif method == 'adaptive_mean':
        blockSize = 11
        shift = 0
        img_bin = cv2.adaptiveThreshold(gray_img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, blockSize, shift).astype('uint8')
        hval = min(max(set(img_bin.flatten())), 255)
        img_out = ((hval-img_bin)/hval).astype('uint8')
    else:
        less_blobs = False
        if not less_blobs:
        #global saf
            img_out = (gray_img < gray_thr).astype('uint8')
            #print('img_out values =  ', set(img_out.flatten()))
        else:
        # get threshold image
        #img_out, thresh_img = cv2.threshold(gray_img, gray_thr, 255, cv2.THRESH_BINARY)
            ret, thresh_img = cv2.threshold(gray_img, gray_thr, 255, cv2.THRESH_BINARY)
            hval = min(max(set(thresh_img.flatten())), 255)
            #print('ret =  ', ret, hval, set(thresh_img.flatten()))
            img_out = ((hval - thresh_img) / hval).astype('uint8')
    #print('img_out bin range values  = ', set(img_out.flatten()))
    return img_out, gray_thr


def img_enhance(img, method='histeq'):
    img_out = img.copy()
    dims = len(img.shape)
    if dims > 2:
        # gray_img = numpy.mean(img, axis=2)
        gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        #img_out = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        #img_out = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        print('img_enhance: convert input image to gray !')
        #img_out = (gray < gray_thr).astype('uint8')
        #return img_out
    else:
        gray_img = img.copy()

    if method == 'orig':
        img_out = gray_img.copy()
    elif method == 'histeq':
        img_out = cv2.equalizeHist(gray_img)
        img_out = cv2.medianBlur(img_out, 7)
    elif method == 'CLAHE':
        # create a CLAHE object (Arguments are optional).
        #createCLAHE(clipLimit = 40.0, tileGridSize = Size(8, 8) )
        clahe = cv2.createCLAHE(clipLimit=40.0, tileGridSize=(250, 250))
        img_out = clahe.apply(gray_img)
    elif method == 'unsharp':
        img_out = unsharp_mask(gray_img, radius=1, amount=1)
    elif method == 'sharp_lap':
        reference = gray_img.copy()
        # img_lap2 = laplace(img_as_float(image))
        img_lap = laplace(gray_img)
        alpha = 1.0
        beta = 1.0
        img_out = match_histograms(beta * gray_img + alpha * img_lap, reference, multichannel=True)
    return img_out

def get_contours(bin_img, run_tree=False):
    if run_tree:
        contours_out, hierarchy = cv2.findContours(bin_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    else:
        contours_out, hierarchy = cv2.findContours(bin_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        #contours, hierarchy = cv2.findContours(bin_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return contours_out, hierarchy

def get_image_contours(img_shape, contours_in, fill_cont=False):
    # create an empty image for contours
    img_contours = np.zeros(img_shape)
    if fill_cont:
        # draw the contours on the empty image
        # drawContours(	image, contours, contourIdx, color[, thickness[, lineType[, hierarchy[, maxLevel[, offset]]]]]	)
        # contourIdx	Parameter indicating a contour to draw. If it is negative, all the contours are drawn.
        # The function draws contour outlines in the image if thickness≥0 or
        # fills the area bounded by the contours if thickness<0 .
        # thickness	Thickness of lines the contours are drawn with.
        # If it is negative (for example, thickness=FILLED ), the contour interiors are drawn.
        return cv2.drawContours(img_contours, contours_in, -1, (255, 255, 255), -1)
    else:
    # draw the contours on the empty image
        return cv2.drawContours(img_contours, contours_in, -1, (0, 255, 0), 3)



def get_convexHull_img(img_contours_diff):
    gray_img = cv2.cvtColor(img_contours_diff.astype('uint8'), cv2.COLOR_RGB2GRAY)
    # The original image is inverted as the object must be white.
    # image = invert(data.horse())
    thresh = int(0.5 * np.max(gray_img) + 0.5)  # 89
    print('get_convexHull_img: thresh ', thresh)
    binary = gray_img >= thresh
    chull = convex_hull_image(binary)
    chull_diff = img_as_float(chull.copy())
    chull_diff[binary] = 2
    return chull_diff, chull, binary

def get_blob_img( img, chull, binary, shft, addfrac=0.05, local_eps=2.22e-16):
    rmin = 0
    cmin = 0
    rmax = img.shape[0]-1
    cmax = img.shape[1]-1
    row_max = rmax
    col_max = cmax
    try:
        #chull = convex_hull_image(binary)
            # create an empty image for contours
        max_size = chull.shape
        row_max = max_size[0]
        col_max = max_size[1]
        rows = np.any(chull, axis=1)
        cols = np.any(chull, axis=0)
        rmin, rmax = np.where(rows)[0][[0, -1]]
        cmin, cmax = np.where(cols)[0][[0, -1]]
        print('hull rmin, rmax, cmin, cmax ', rmin, rmax, cmin, cmax)
        # print(rmax-rmin,cmax-cmin,  rows, cols, int(511*(1.0+20/511)+0.5), int(786*(1.0+20/786)+0.5))
        # out = [[205, 690], [1010, 1220]]

    except Exception as inst:
        print("get_blob_img: convex hull had exception on : ", inst)
        print('try direct binary ...binary.shape = ', binary.shape)
        if binary is not None:
            max_size = binary.shape
            row_max = max_size[0]
            col_max = max_size[1]
            rows = np.any(binary == True or binary == 1, axis=1)
            cols = np.any(binary == True or binary == 1, axis=0)
            rmin, rmax = np.where(rows)[0][[0, -1]]
            cmin, cmax = np.where(cols)[0][[0, -1]]
            print('binary rmin, rmax, cmin, cmax ', rmin, rmax, cmin, cmax)
    add_pixs = int(0.2*addfrac*(rmax-rmin))
    rmin = max(int(rmin * (1.0 - add_pixs / max(rmin, local_eps)) - 0.5), 0)
    rmax = min(int(rmax * (1.0 + add_pixs / max(rmax, local_eps)) + 0.5), row_max)
    cmin = max(int(cmin * (1.0 - add_pixs / max(cmin, local_eps)) - 0.5), 0)
    cmax = min(int(cmax * (1.0 + add_pixs / max(cmax, local_eps)) + 0.5), col_max)
    rmin = max(int(rmin - addfrac * rmin - 0.5), 0)
    rmax = min(int(rmax + addfrac * rmax + 0.5), row_max)
    cmin = max(int(cmin - addfrac * cmin - 0.5), 0)
    cmax = min(int(cmax + addfrac * cmax + 0.5), col_max)
    print('2new rmin, rmax, cmin, cmax ...', rmin, rmax, cmin, cmax)

    # img_blob = np.zeros((rows, cols, img.shape[2]))
    # print(img.shape, img_blob.shape, type(img_blob))
    rmin = max(shft[1]+rmin, 0)
    cmin = max(shft[0]+cmin, 0)
    rmax = min(shft[1]+rmax + 1, img.shape[0]-1)
    cmax = min(shft[0]+cmax + 1, img.shape[1]-1)
    img_blob = img[rmin:rmax, cmin:cmax].copy()
    #cv2.imwrite('debug_img_blob.jpg', img_blob)
    minmax_vec = (rmin, rmax, cmin, cmax)
    return img_blob, minmax_vec

